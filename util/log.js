/**
 * Created by kio on 7/17/16.
 */

const winston = require('winston');

if (process.env.NODE_ENV == 'development') {
    winston.loggers.options.transports = [
        new (winston.transports.Console)({name: 'stdout', level: 'debug', timestamp: true})
    ];
}

if (process.env.NODE_ENV == 'production') {
    winston.loggers.options.transports = [
        new (winston.transports.Console)({name: 'stdout', level: 'debug', timestamp: true})
    ];
}

let log = new (winston.Logger)();

module.exports = function(label, opts) {
    "use strict";
    if (!label || label.length == 0) {
        return log;
    }

    winston.loggers.add(label, opts);

    return winston.loggers.get(label);
};