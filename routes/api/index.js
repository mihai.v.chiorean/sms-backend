'use strict';

const express = require('express');
const router = express.Router();
const mqtt = require('mqtt');
const QCli = require('../../clients/mqttclient');

const log = require('../../util/log')('API-index');

// var client = mqtt.connect('mqtt://gkvjzvbk:tFr-vZeNNW4m@m10.cloudmqtt.com:17269');

QCli.subscribe('presence', (msg) => {
    log.info('New device present', {channel: 'presence', message: msg})
});

router.post('/presence', function(req, res, next) {
  log.info('New device present', {body: JSON.stringify(req.body)});
  QCli.post('presence', req.body, () => {
      log.info('Device queued');
      res.status(201).send({msg: 'Device advertised!'});
  });
});

router.post('/message', function(req, res, next) {
    log.info('Posting message', {body: req.body});
    QCli.post('message', req.body, function() {
        log.info('Message posted')
    });
    res.status(201).send({msg: 'Posted'});
});

module.exports = router;
