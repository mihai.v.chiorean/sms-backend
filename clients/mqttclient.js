/**
 * Created by kio on 7/17/16.
 */
'use strict';

const mqtt = require('mqtt');
const async = require('async');
const log = require('../util/log')('MqttClient');

const SSL_MQTTURI = 'mqtts://ktkyohif:iVuDEG9BYS4m@m12.cloudmqtt.com:24591';
const MQTTURI = 'mqtt://ktkyohif:iVuDEG9BYS4m@m12.cloudmqtt.com:14591';

class MqttClient {
    constructor(mqttUri) {
        var self = this;
        this._router = {};
        this.client = mqtt.connect(mqttUri);

        this.client.on('connect', function () {
            log.debug('Client connected', {timestamp: new Date()});
            self.client.on('message', self._route.bind(self));
        });

        this.client.on('reconnect', () => {
            log.debug('Reconnected');
        });

        this.client.on('offline', () => {
            log.log('warn', 'Client is offline')
        });

        this.client.on('error', (err) => {
            log.error(err)
        });

        this.client.on('close', () => {
            log.debug('Closed');
        });
    }

    /**
     * Private methods
     */
    _route(channel, message, packet) {
        log.debug('Message received. Routing message to handlers', {channel: channel, message: message});

        (this._router[channel] || []).forEach((f) => {
            f(message, packet);
        });
    }

    /**
     * Public methods
     */
    subscribe(channel, handler) {
        log.debug('Subscribing to channel', {channel: channel});
        let self = this;
        this.client.subscribe(channel, (err, granted) => {
            if (err) {
                log.error('Could not subscribe to channel', {channel: channel, error: err});
                return
            }
            if (self._router[channel] == null) {
                self._router[channel] = [];
            }

            self._router[channel].push(handler);

            log.info('Subscribed to channel', {channel: channel, granted: granted})
        });
    }

    close() {
        log.debug('Close requested');
        this.client.end();
    }

    post(channel, pack, cb) {
        log.debug('Posting', {pack: pack});

        let publish = this.client.publish.bind(this.client, channel, JSON.stringify(pack), {qos: 1});
        // try calling apiMethod 3 times, waiting 200 ms between each retry
        async.retry({times: 3, interval: 1000}, publish, function(err, result) {
            log.error('Failed to post message', err);
            // do something with the result
            log.debug('Posted message', {channel: channel});
            log.debug('Posted message', result);
            if (cb) {
                log.debug('has cb');
                return cb();
            }
        });
    }
}

var cli = new MqttClient(MQTTURI);

module.exports = cli;