'use strict';

var tap = require('tape');
const supertest = require('supertest');

var api  = supertest(require('../app'));

tap.test('POST /api/presence', t => {
    t.plan(2);

    api.post('/api/presence')
        .expect('Content-Type', /json/)
        .expect(201)
        .end((err, res) => {
            console.log('got a reply')
            if (err) throw err;

            t.end();
            console.log('test ended')

        });
}, {
    timeout: 500
});